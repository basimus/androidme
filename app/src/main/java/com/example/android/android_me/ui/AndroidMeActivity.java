package com.example.android.android_me.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

// This activity will display a custom Android image composed of three body parts: head, body, and legs
public class AndroidMeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_me);


      if(savedInstanceState == null) {
          BodyPartFragment headFragment = new BodyPartFragment();
          BodyPartFragment bodyFragment = new BodyPartFragment();
          BodyPartFragment legFragment = new BodyPartFragment();
          FragmentManager fragmentManager = getSupportFragmentManager();


          headFragment.setImageIds(AndroidImageAssets.getHeads());
          headFragment.setListIndex(1);
          addFragment(fragmentManager, headFragment, R.id.head_container);

          bodyFragment.setImageIds(AndroidImageAssets.getBodies());
          bodyFragment.setListIndex(1);
          addFragment(fragmentManager, bodyFragment, R.id.body_container);

          legFragment.setImageIds(AndroidImageAssets.getLegs());
          legFragment.setListIndex(1);
          addFragment(fragmentManager, legFragment, R.id.leg_container);
      }

    }

    private void addFragment(FragmentManager fragmentManager, Fragment fragment, int id){
        fragmentManager.beginTransaction()
                .add(id, fragment)
                .commit();
    }
}